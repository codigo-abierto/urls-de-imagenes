const imagenes = document.querySelectorAll('.container--MwyXl .link--WHWzm img');
const urls = Array.from(imagenes).map(function(img){
    return img.src;
});
console.table(urls);
// Convertir el array de URLs a un string separado por saltos de línea
const urlsText = urls.join('\n');
// Mostrar el texto para verificar
console.table(urlsText);


// Paso 2 y 3: Crear un archivo de texto y descargarlo
const blob = new Blob([urlsText], { type: 'text/plain' }); // Crea un Blob con el texto de las URLs
const url = URL.createObjectURL(blob); // Crea una URL para el Blob

// Crea un elemento 'a' temporal para facilitar la descarga
const downloadLink = document.createElement('a');
downloadLink.href = url; // Asigna la URL del Blob al href del enlace
downloadLink.download = 'image-urls.txt'; // Asigna el nombre de archivo deseado
document.body.appendChild(downloadLink); // Agrega el enlace al documento
downloadLink.click(); // Simula un clic para iniciar la descarga
document.body.removeChild(downloadLink); // Elimina el enlace del documento